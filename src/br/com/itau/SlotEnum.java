package br.com.itau;

public enum SlotEnum {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    BAR(900),
    SETE(300);
    //...
    private int valor;

    SlotEnum(int valor){
        this.valor = valor;
    }

    public int getValor(){
        return this.valor;
    }
}
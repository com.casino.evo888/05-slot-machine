package br.com.itau;

import java.util.ArrayList;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	// write your code here
        IO io = new IO();
        io.imprimeMsgInicial();

        Slot slot = new Slot(io.obterQuantidadeSlots(), 0);
        boolean fim = false;
        while(!fim) {
            io.imprimePontuacao(slot.getPontuacao());
            String acao = io.getAcao();

            if(acao.equals("jogar")){
                slot.jogar();
            } else
            if(acao.equals("sair")){
                fim = true;
            }
        }

        io.imprimePontuacao(slot.getPontuacao());
    }
}

package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Slot {
    private int quantidadeSlots;
    private long pontuacao;

    public Slot(int quantidadeSlots, long pontuacao) {
        this.quantidadeSlots = quantidadeSlots;
        this.pontuacao = pontuacao;
    }

    public int getQuantidadeSlots() {
        return quantidadeSlots;
    }

    public void setQuantidadeSlots(int quantidadeSlots) {
        this.quantidadeSlots = quantidadeSlots;
    }

    public long getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(long pontuacao) {
        this.pontuacao = pontuacao;
    }

    public void jogar(){
        Sorteador sorteador = new Sorteador();
        List<SlotEnum> sorteio = new ArrayList<>();

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < this.getQuantidadeSlots(); i++){
            SlotEnum s = sorteador.sorteiaSlot();
            sorteio.add(s);
            this.pontuacao += s.getValor();
            sb.append("|" + s + " " + s.getValor());
        }
        sb.append("|");
        System.out.println(sb.toString());
    }
}

package br.com.itau;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimeMsgInicial(){
        System.out.println("Bem vindo ao elevador");
    }

    public static String getAcao(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("O que deseja fazer? (jogar/sair) ");

        return scanner.nextLine();
    }

    public static void imprimePontuacao(long pontuacao){
        System.out.println("Pontuacao: " + pontuacao);
    }

    //DTO - data transfer object
    public static Map<String, Integer> obterElevador(){
        Scanner scanner = new Scanner(System.in);

        Map<String, Integer> elevador = new HashMap<>();

        System.out.println("Numeros de andares: ");
        int andares = scanner.nextInt();
        System.out.println("Capacidade: ");
        int capacidade = scanner.nextInt();

        elevador.put("andares", andares);
        elevador.put("capacidade", capacidade);

        return elevador;
    }

    public static void imprimeErro(String msg){
        System.out.println(msg);
    }

    public int obterQuantidadeSlots() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Quantos slots por jogo? ");

        return scanner.nextInt();
    }

}
